package nl.codeyellow.bgs.nightlydropper;
import com.red_folder.phonegap.plugin.backgroundservice.BackgroundService;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.database.sqlite.SQLiteDatabase;
import android.app.PendingIntent;
import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DropTimer extends BackgroundService {

    private final static String TAG = DropTimer.class.getSimpleName();
    private String mHelloTo = "World";

    @Override
    protected JSONObject doWork() {
        JSONObject result = new JSONObject();

        Intent alarmIntent = new Intent(this, SqliteDropper.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Calendar triggerTime = Calendar.getInstance();
        triggerTime.set(Calendar.HOUR_OF_DAY, 23);
        triggerTime.set(Calendar.MINUTE, 59);
        triggerTime.set(Calendar.SECOND, 0);
        alarmManager.set(AlarmManager.RTC, triggerTime.getTimeInMillis(), pendingIntent);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String then = df.format(triggerTime.getTime());
        Log.d(TAG, "Setting alarm at " + then);
        stopSelf();
        return result;
    }

    @Override
    protected JSONObject getConfig() {
        JSONObject result = new JSONObject();

        try {
            result.put("HelloTo", this.mHelloTo);
        } catch (JSONException e) {
        }

        return result;
    }

    @Override
    protected void setConfig(JSONObject config) {
        try {
            if (config.has("HelloTo"))
                this.mHelloTo = config.getString("HelloTo");
        } catch (JSONException e) {
        }

    }

    @Override
    protected JSONObject initialiseLatestResult() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected void onTimerEnabled() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onTimerDisabled() {
        // TODO Auto-generated method stub

    }
}
