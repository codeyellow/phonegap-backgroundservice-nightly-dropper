package nl.codeyellow.bgs.nightlydropper;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.Cursor;

import android.util.Log;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;

import java.util.List;

import android.os.Process;

public class SqliteDropper extends BroadcastReceiver {

    private final static String TAG = "DropTimer";
    private static String DB_NAME = "addressbased";

    @Override
    public void onReceive(final Context context, Intent intent) {
        SQLiteDatabase db = context.openOrCreateDatabase(DB_NAME, context.MODE_WORLD_WRITEABLE, null);
        db.execSQL("DELETE FROM regular_donors");
        db.execSQL("DELETE FROM encrypted_donors");
        db.execSQL("DELETE FROM recruitment_config");
        db.execSQL("DELETE FROM districts");
        db.execSQL("DELETE FROM recruiters");
        db.execSQL("DELETE FROM donor_locations");
        Log.d(TAG, "Truncrating tables...");

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> pids = am.getRunningAppProcesses();

        // Find the parent PROCESS of this service
        for(int i = 0; i < pids.size(); i++) {
            ActivityManager.RunningAppProcessInfo info = pids.get(i);
            if (context.getPackageName().contains(info.processName)) {
                Log.d(TAG, "Remote process name: " + context.getPackageName());
                Log.d(TAG, "Process name: " + info.processName + " PID: " + info.pid);
                Process.killProcess(info.pid);
            }
        }
    }
}
